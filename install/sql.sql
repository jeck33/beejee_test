CREATE TABLE `tasks` (
  `id` bigint(20) NOT NULL,
  `name` varchar(250) NOT NULL DEFAULT '',
  `email` varchar(150) NOT NULL DEFAULT '',
  `text` text,
  `status` smallint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `tasks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`),
  ADD KEY `email` (`email`);

ALTER TABLE `tasks`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;