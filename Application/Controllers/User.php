<?php

namespace Application\Controllers;

use Application\Models;

class User extends Models\View{
    
    private $request;
    private $user;
    
    function __construct() {
        $this->request = new Models\Request();
        $this->user = new \Application\Models\User();
    }
    
    function auth()
    {
        $this->user->auth();
        $this->request->redirect('/');
    }
    
    function logout()
    {
        $this->user->logout();
        $this->request->redirect('/');
    }
    
}
