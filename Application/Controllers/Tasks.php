<?php

namespace Application\Controllers;

use Application\Models\View;
use Application\Models\User;

class Tasks extends View{
    
    private $request;
    
    private $modelFields = [
        'id', 'name', 'email', 'text', 'status'
    ];
    
    function __construct() {
        $this->request = new \Application\Models\Request();
        $this->pageTitle = 'Задачи';
    }
    
    function auth()
    {
        if($this->request->user === 'admin' && $this->request->password === '123'){
            $_SESSION['User'] = 'admin';
        }
    }
    
    function logout()
    {
        session_destroy();
    }
    
    function index($page = null, $page_number = null, $sort = null){
        $page = 1;
        if(!is_null($page_number) && is_numeric($page_number)){
            $page = (int)$page_number;
        }
        $this->pageTitle .= ' | Список';
        $tasks = new \Application\Models\Tasks();
        $list = $tasks->getList($page, $sort);
        
        $paginator = new \Application\Models\Paginator(
            $tasks->getCount(), 
            $tasks::PER_PAGE, 
            (int)$this->request->page, 
            '/tasks/index/page/(:num)' . ((!is_null($sort)) ? '/' . htmlspecialchars($sort) : '')
        );
        
        $this->draw_view([
            'list' => $list,
            'page' => $page,
            'sort' => $sort,
            'pagination' => $paginator,
        ]);
    }
    
    function add(){
        $add_data = ['status' => 0];
        foreach($this->modelFields as $field){
            if($field == 'id' || $field == 'status'){ continue; }
            if(is_null($this->request->$field)){
                Throw new \Exception('Данные для добавления не корректны! Не передан параметр: ' . $field);
            }
            $add_data[$field] = $this->request->{$field};
        }
        $tasks = new \Application\Models\Tasks();
        $tasks->add($add_data);
        
        $this->request->redirect();
    }
    
    function edit($status_text = null, $status_id = null, $page = 1){
        if(User::check_auth() !== true){
            $this->request->redirect('/tasks/index/');
        }
        $tasks = new \Application\Models\Tasks();
        if($this->request->id && $this->request->text){
            $tasks->setText((int)$this->request->id, $this->request->text);
            $page = $this->request->page;
        }
        if($status_id){
            $tasks->setStatus($status_id, 1);
        }
        if((int)$page > 1){
            $this->request->redirect('/tasks/index/page/' . $page);
        } else {
            $this->request->redirect('/tasks/index/');
        }
    }
    
    function not_found()
    {
        $this->pageTitle = 'Страница не найдена';
        header("HTTP/1.0 404 Not Found");
        $this->draw_view();
    }
    
}
