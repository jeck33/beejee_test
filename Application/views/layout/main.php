<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>BeeJee test project<?=(isset($this->pageTitle) ? ' | ' . $this->pageTitle : '')?></title>

    <!-- Bootstrap core CSS -->
    <link href="/css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="/css/style.css" rel="stylesheet">
    
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    
  </head>

  <body class="bg-light">

    <nav class="navbar navbar-expand-md fixed-top navbar-dark bg-dark">
      <a class="navbar-brand" href="#">BeeJee test project</a>
      <button class="navbar-toggler p-0 border-0" type="button" data-toggle="offcanvas">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="navbar-collapse offcanvas-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link" href="/">Задачи <span class="sr-only">(current)</span></a>
          </li>
        </ul>
          <?php if(Application\Models\User::check_auth() === true){ ?>
          <button type="button" class="btn btn-primary" onclick="document.location.href = '/user/logout'">
            Выйти
          </button>
          <?php } else { ?>
          <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#authModal">
            Авторизация
          </button>
            
          <?php } ?>
      </div>
    </nav>

    <main role="main" class="container-fluid">
        <?php $this->get_content(); ?>
    </main>

<!-- Modal -->
            <div class="modal fade" id="authModal" tabindex="-1" role="dialog" aria-labelledby="authModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="authModalLabel">Авторизация</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                    <form action="/user/auth" method="post">
                        <div class="modal-body">
                              <div class="form-group">
                                <label for="authInputEmail1">Email</label>
                                <input name="user" type="text" class="form-control" id="authInputEmail1" placeholder="Логин">
                              </div>
                              <div class="form-group">
                                <label for="authInputPassword1">Пароль</label>
                                <input name="password" type="password" class="form-control" id="authInputPassword1" placeholder="Пароль">
                              </div>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                          <button type="submit" class="btn btn-primary">Войти</button>
                        </div>
                    </form>
                </div>
              </div>
            </div>
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <script src="/js/popper.js"></script>
    <script src="/js/bootstrap.js"></script>
    <script src="/js/holder.js"></script>
  </body>
</html>

