<div class="d-flex align-items-center p-3 my-3 text-white-50 bg-purple rounded box-shadow">
<div class="lh-100">
  <h6 class="mb-0 text-white lh-100">Задачи</h6>
  <small></small>
</div>
</div>

 <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addModal">
    Добавить
</button>
Сортировка 
<select class="form-control form-control-sm tasks_order col-sm-3" style="display: inline-block">
    <option value="id_desc">По порядку</option>
    <option <?=($sort === 'email_asc') ? 'selected ' : ''?>value="email_asc">Email по алфавиту</option>
    <option <?=($sort === 'email_desc') ? 'selected ' : ''?>value="email_desc">Email по алфавиту в обратном порядке</option>
    <option <?=($sort === 'name_asc') ? 'selected ' : ''?>value="name_asc">Имя по алфавиту</option>
    <option <?=($sort === 'name_desc') ? 'selected ' : ''?>value="name_desc">Имя по алфавиту в обратном порядке</option>
    <option <?=($sort === 'status_desc') ? 'selected ' : ''?>value="status_desc">Статус (сначала  завершенные)</option>
    <option <?=($sort === 'status_asc') ? 'selected ' : ''?>value="status_asc">Статус (сначала не завершенные)</option>
</select>

<div class="my-3 p-3 bg-white rounded box-shadow">
<?php
foreach($list as $row){
    ?>
    <div class="media text-muted pt-3">
        <img data-src="holder.js/32x32?theme=thumb&bg=007bff&fg=007bff&size=1" alt="" class="mr-2 rounded">
        <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
          <strong class="d-block text-gray-dark">
            <?=$row['name']?> (<?=$row['email']?>)
            <?php if(Application\Models\User::check_auth() === true){ ?>
            <button type="button" class="btn-edit-task btn btn-sm" data-id="<?=$row['id']?>" data-toggle="modal" data-target="#editModal">
                Редактировать текст
            </button>
            <?php } ?>
            <span>
            <?php if($row['status'] == 1){ ?>
                <span class="badge badge-secondary">Закрыто</span>
            <?php } else { ?>
                <?php if(Application\Models\User::check_auth() === true){ ?>
                <button type="button" class="btn btn-sm" onclick="document.location.href='/tasks/edit/status/<?=$row['id']?>/<?=$page?>'">
                    Выполнено
                </button>
                <?php } ?>
            <?php } ?>
            </span>
          </strong>
            <span class="edit_text"><?= htmlspecialchars($row['text'])?></span>
        </p>
    </div>
    <?php
}
?>
</div>
<?php
echo $pagination;
?>
<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="addModalLabel">Добавление задачи</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <form action="/tasks/add" method="post">
            <div class="modal-body">
                <!-- @TODO jfs csrf check add -->
                  <div class="form-group">
                    <label for="addInputEmail1">Email</label>
                    <input name="email" type="email" class="form-control" id="addInputEmail1" aria-describedby="emailHelp" placeholder="Email">
                  </div>
                  <div class="form-group">
                    <label for="addInputName">ФИО</label>
                    <input name="name" type="text" class="form-control" id="addInputName" placeholder="ФИО">
                  </div>
                 <div class="form-group">
                    <label for="addInputText">Текст</label>
                    <textarea name="text" class="form-control" id="addInputText" placeholder="Текст"></textarea>
                  </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
              <button type="submit" class="btn btn-primary">Добавить</button>
            </div>
        </form>
    </div>
  </div>
</div>

<script>
    $('.btn-edit-task').click(function(){
        $('#editModal').find('.edit_id').val($(this).data('id'));
        $('#editInputText').text($(this).closest('.media').find('.edit_text').text());
    });
    $('.tasks_order').change(function(){
        let sort_ = $(this).val();
        document.location.href = '/tasks/index/page/<?=$page?>/' + sort_;
    });
</script>

<?php if(Application\Models\User::check_auth() === true){ ?>
  <!-- Modal -->
  <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="editModalLabel">Редактирвание задачи</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
          <form action="/tasks/edit" method="post">
              <input class="page" type="hidden" name="page" value="<?=(int)$page?>" />
              <input class="edit_id" type="hidden" name="id" value="" />
              <div class="form-group">
                <label for="editInputText">Текст</label>
                <textarea name="text" class="form-control" id="editInputText" placeholder="Текст"></textarea>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                <button type="submit" class="btn btn-primary">Сохранить</button>
              </div>
          </form>
      </div>
    </div>
  </div>
<?php } ?>