<?php

namespace Application\Classes;

class Config {
    
    private $main;
    private $db;
    
    /**
     *
     * @var Config 
     */
    private static $_instance;
    
    /**
     * 
     */
    private function __construct()
    {
        $this->main = require(APP_PATH . '/Application/config/config.php');
        $this->db = require(APP_PATH . '/Application/config/config.db.php');
    }
    
    /**
     * 
     * @return \Config
     */
    public static function getInstance()
    {
        if(is_null(self::$_instance)){
            self::$_instance = new self();
        }
        
        return self::$_instance;
    }
    
    /**
     * 
     * @param string $attr
     * @return mixed
     */
    public function db($attr = null)
    {
        if(!is_null($attr)){
            return $this->db[$attr];
        }
        
        return $this->db;
    }
    
    /**
     * 
     * @param string $attr
     * @return mixed
     */
    public function main($attr = null)
    {
        if(!is_null($attr)){
            return $this->main[$attr];
        }
        
        return $this->main;
    }
}