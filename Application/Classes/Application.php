<?php

namespace Application\Classes;

class Application {
    private $controller = null;
    private $action = null;
    private $params = array();
    private $namespace = null;
    
    public static $data = [];

    public function __construct() {
        session_start();
        
        $return_404 = false;
        
        $this->splitUrl();
        
        $this->namespace = 'Application\\Controllers\\';

        if (!$this->controller) {
            self::$data = [
                'controller' => 'tasks',
                'action' => 'index',
            ];
            $page = new \Application\Controllers\Tasks();
            $page->index();

        } elseif (file_exists(APP_PATH . '/Application/Controllers/' . ucfirst ($this->controller) . '.php')) {
            //require APP_PATH . '/application/controllers/' . $this->controller . '.php';
            $controller = $this->namespace . ucfirst($this->controller);
            $this->controller = new $controller();
            
            self::$data = [
                'controller' => 'tasks',
            ];

            if (method_exists($this->controller, $this->action)) {
                self::$data['action'] = $this->action;
                if (!empty($this->params)) {
                    $res = call_user_func_array(array($this->controller, $this->action), $this->params);
                } else {
                    $this->controller->{$this->action}();
                }

            } else {
                self::$data['action'] = 'index';
                if (strlen($this->action) == 0) {
                    $this->controller->index();
                }
                else {
                    die('123');
                    $return_404 = true;
                }
            }
        } else {
            die('12');
            $return_404 = true;
        }
        
        if($return_404){
            $page = new \Application\Controllers\Tasks();
            self::$data = [
                'controller' => 'tasks',
                'action' => 'not_found',
            ];
            $res = $page->not_found();
        }
    }

    private function splitUrl() {
        if (isset($_GET['url'])) {
            $url = explode('/', filter_var(trim($_GET['url'], '/'), FILTER_SANITIZE_URL));

            $this->controller = isset($url[0]) ? ucfirst(strtolower(basename($url[0]))) : null;
            $this->action = isset($url[1]) ? basename($url[1]) : null;

            unset($url[0], $url[1]);

            $this->params = array_values($url);
        }
    }
}

