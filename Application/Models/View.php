<?php

namespace Application\Models;

use \Application\Classes\Application;

class View {
    
    private $template;
    private $params;
    
    function draw_view($params = [], $template = ''){
        $this->template = $template;
        $this->params = $params;
        extract($params);
        require APP_PATH . '/Application/views/layout/main.php';
        
    }
    
    private function get_content()
    {
        extract($this->params);
        require APP_PATH 
            . '/Application/views/' 
            . Application::$data['controller'] 
            . '/' . (($this->template !== '') ? $this->template : Application::$data['action']) . '.php';
    }
    
}
