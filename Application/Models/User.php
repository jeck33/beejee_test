<?php

namespace Application\Models;

class User {
    function __construct() {
        $this->request = new Request();
    }
    
    function auth()
    {
        $res = false;
        if($this->request->user === 'admin' && $this->request->password === '123'){
            $_SESSION['User'] = 'admin';
            $res = true;
        }
        
        return $res;
    }
    
    static function check_auth()
    {
        return isset($_SESSION['User']) && $_SESSION['User'] === 'admin';
    }
    
    function logout()
    {
        session_destroy();
    }
}
