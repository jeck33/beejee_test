<?php

namespace Application\Models;

use Application\Models\DB;

class Tasks{
    
    const PER_PAGE = 3;
    private $totalPages = 1;
    private $totalCount;
    
    function __construct() {
        
    }
    
    function getCount()
    {
        $result = 0;
        if(!is_null($this->totalCount)){
            $result = $this->totalCount;
        } else {
            $res = DB::getInstance()->getValue('SELECT COUNT(*) FROM tasks');
            if($res){
                $this->totalCount = $result = $res;
            }
        }
        
        return $result;
    }
    
    function getList($page, $sort = '')
    {
        $sort = $this->_prepareSort($sort);
        $limits = $this->_prepareLimits($page);
        $result = [];
        $res = DB::getInstance()
                ->getRows('SELECT * FROM tasks ORDER BY ' . $sort . $limits);
        if($res){
            $result = $res;
        }
        
        return $result;
    }
    
    function getOne($id)
    {
        $result = [];
        $res = DB::getInstance()->getRow('SELECT * FROM tasks WHERE id = ?', [$id]);
        if($res){
            $result = $res;
        }
        
        return $result;
    }
    
    function add($data)
    {
        $query = "INSERT INTO `tasks` (
          `name`,
          `email`,
          `text`,
          `status`
          )
        VALUES (
          :name,
          :email,
          :text,
          :status
        )";

        $args = [
          'name' => $data['name'],
          'email' => $data['email'],
          'text' => $data['text'],
          'status' => $data['status'],
        ];
        
        DB::getInstance()->sql($query, $args);
    }
    
    function update($id, $data)
    {
        $query = "UPDATE `tasks`
            SET `name` = :name, `email` = :email, `text` = :text, `status` = :status
            WHERE `id` = :id";

        $args = [
          'name' => $data['name'],
          'email' => $data['email'],
          'text' => $data['text'],
          'status' => $data['status'],
          'id' => $id,
        ];

        DB::getInstance()->sql($query, $args);
    }
    
    function setText($id, $text)
    {
        $query = "UPDATE `tasks`
            SET `text` = :text
            WHERE `id` = :id";

        $args = [
          'text' => $text,
          'id' => $id,
        ];

        DB::getInstance()->sql($query, $args);
    }
    
    function setStatus($id, $status)
    {
        $query = "UPDATE `tasks`
            SET `status` = :status
            WHERE `id` = :id";

        $args = [
          'status' => $status,
          'id' => $id,
        ];

        DB::getInstance()->sql($query, $args);
    }
    
    private function _prepareLimits($page)
    {
        $result = '';
        $count = $this->getCount();
        $this->totalPages = ceil($count / self::PER_PAGE);
        if($count <= self::PER_PAGE || $page === 1){
            $result .= ' LIMIT ' . self::PER_PAGE;
        } else {
            $result .= ' LIMIT ' . self::PER_PAGE;
            $result .= ' OFFSET ' . (int)(self::PER_PAGE * ($page - 1));
        }
        
        return $result;
    }
    
    private function _prepareSort($sort)
    {
        $result = ' `id` DESC ';
        if(!is_null($sort)){
            $_ = explode('_', $sort);
            if(count($_) === 2){
                if(in_array(strtolower($_[0]), ['email', 'name', 'status']) && in_array(strtolower($_[1]), ['asc', 'desc'])){
                    $result = ' `' . strtolower($_[0]) . '`  ' . strtolower($_[1]);
                }
            }
        }
        
        return $result;
    }
    
    function getPages()
    {
        return $this->totalPages;
    }
    
}
