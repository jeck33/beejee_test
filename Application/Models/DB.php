<?php

namespace Application\Models;

use Application\Classes\Config;

/**
 * Class wrapper for working with PDO
 */
class DB
{
 
  /**
   * @var PDO
   */
  private $db;
 
  /**
   * @var null
   */
  protected static $instance = null;
 
  /**
   * DB constructor.
   * @throws Exception
   */
  private function __construct(){
    $config = Config::getInstance()->db();
    
    try {
      $this->db = new \PDO(
        'mysql:host='.$config['host'].';dbname='.$config['name'],
        $config['user'],
        $config['password'],
        $options = [
            \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
            \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
            \PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES ".$config['charset']
        ]
      );
    } catch (PDOException $e) {
        throw new Exception ($e->getMessage());
    }
    
  }
  
  public static function getInstance()
  {
    if (self::$instance === null){
        self::$instance = new self();
    }
    return self::$instance;
  }
 
  /**
   * @param $stmt
   * @return PDOStatement
   */
  public function query($stmt)  {
    return $this->db->query($stmt);
  }
 
  /**
   * @param $stmt
   * @return PDOStatement
   */
  public function prepare($stmt)  {
    return $this->db->prepare($stmt);
  }
 
  /**
   * @param $query
   * @return int
   */
  public function exec($query) {
    return $this->db->exec($query);
  }
 
  /**
   * @return string
   */
  public function lastInsertId() {
    return $this->db->lastInsertId();
  }
 
  /**
   * @param $query
   * @param array $args
   * @return PDOStatement
   * @throws Exception
   */
  public function run($query, $args = [])  {
    try{
      if (!$args) {
        return $this->query($query);
      }
      $stmt = $this->prepare($query);
      $stmt->execute($args);
      return $stmt;
    } catch (PDOException $e) {
        throw new Exception($e->getMessage());
    }
  }
 
  /**
   * @param $query
   * @param array $args
   * @return mixed
   */
  public function getRow($query, $args = [])  {
    return $this->run($query, $args)->fetch();
  }
 
  /**
   * @param $query
   * @param array $args
   * @return array
   */
  public function getRows($query, $args = [])  {
    return $this->run($query, $args)->fetchAll();
  }
 
  /**
   * @param $query
   * @param array $args
   * @return mixed
   */
  public function getValue($query, $args = [])  {
    $result = $this->getRow($query, $args);
    if (!empty($result)) {
      $result = array_shift($result);
    }
    return $result;
  }
 
  /**
   * @param $query
   * @param array $args
   * @return array
   */
  public function getColumn($query, $args = [])  {
    return $this->run($query, $args)->fetchAll(PDO::FETCH_COLUMN);
  }
 
  public function sql($query, $args = [])
  {
    $this->run($query, $args);
  }
  
}
