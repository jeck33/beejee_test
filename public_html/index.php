<?php

/* 
 * jfs MInMVC
 */

use Application\Classes;

define('APP_PATH', realpath(dirname(__FILE__) . '/../')); // тут бессмысленно, но позвоялет перенести ядро в другое место

spl_autoload_register(function ($class) {
    $file = APP_PATH . '/' . str_replace('\\', '/', $class) . '.php';
    if (file_exists($file)) {
        require $file;
    }
});

//Classes\Config::getInstance()->db();
//Classes\Config::getInstance()->main();

new Classes\Application;